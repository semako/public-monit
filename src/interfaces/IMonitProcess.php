<?php

namespace semako\monit\interfaces;

/**
 * Interface IMonitProcess
 * @package semako\monit\interfaces
 */
interface IMonitProcess
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return int
     */
    public function getName();

    /**
     * @return int
     */
    public function getStatus();

    /**
     * @return int
     */
    public function getMonitoring();

    /**
     * @return int|null
     */
    public function getPid();

    /**
     * @return string|null
     */
    public function getPidFile();

    /**
     * @return bool
     */
    public function isRunning();

    /**
     * @return bool
     */
    public function isStarting();

    /**
     * @return bool
     */
    public function isStopped();

    /**
     * @return void
     */
    public function stop();

    /**
     * @return void
     */
    public function start();

    /**
     * @return void
     */
    public function restart();

    /**
     * @return void
     */
    public function monitor();

    /**
     * @return void
     */
    public function unMonitor();
}
