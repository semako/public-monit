<?php

namespace semako\monit\interfaces;

/**
 * Interface IMonit
 * @package semako\monit\interfaces
 */
interface IMonit
{
    /**
     * @return string
     */
    public function getSysConfigFile();

    /**
     * @return string
     */
    public function getInstances();

    /**
     * @param string $sysConfigFile
     */
    public function setSysConfigFile($sysConfigFile);

    /**
     * @param string $instances
     */
    public function setInstances($instances);

    /**
     * @return void
     */
    public function stopAll();

    /**
     * @return void
     */
    public function startAll();

    /**
     * @return void
     */
    public function reload();

    /**
     * @return void
     */
    public function writeConfig();
}
