<?php

namespace semako\monit\traits;

use semako\monit\interfaces\IMonit;
use yii;

/**
 * Class EventsManager
 * @package semako\monit\traits
 * @property IMonit $monit
 */
trait Monit
{
    /**
     * @return IMonit
     */
    public function getMonit()
    {
        return Yii::$app->monit;
    }
}
