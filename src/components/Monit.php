<?php

namespace semako\monit\components;

use semako\monit\interfaces\IMonit;
use semako\monit\interfaces\IMonitProcess;
use yii;
use yii\base\Component;

/**
 * Class Monit
 * @package semako\monit\components
 */
class Monit extends Component implements IMonit
{
    /**
     * @var string
     */
    public $sysConfigFile;

    /**
     * @var string
     */
    public $instances;

    /**
     * @return string
     */
    public function getSysConfigFile()
    {
        return $this->sysConfigFile;
    }

    /**
     * @param string $sysConfigFile
     */
    public function setSysConfigFile($sysConfigFile)
    {
        $this->sysConfigFile = $sysConfigFile;
    }

    /**
     * @return string
     */
    public function getInstances()
    {
        return $this->instances;
    }

    /**
     * @param string $instances
     */
    public function setInstances($instances)
    {
        $this->instances = $instances;
    }

    /**
     * @return IMonitProcess[]
     */
    private function getRunningProcesses()
    {
        if (!$this->isRunning()) {
            return [];
        }

        /* @var IMonitProcess[] $processes */
        $processes = [];
        $items     = explode(PHP_EOL . PHP_EOL, trim(shell_exec('monit status')));

        foreach ($items as $item) {
            if (!preg_match('/^Process \'queue([0-9]+)\'/is', $item, $match)) {
                continue;
            }

            $pid        = null;
            $id         = $match[1];
            $status     = null;
            $monitoring = null;

            if (preg_match('/status\s+(Not monitored|Running|Initializing \- restart pending|Initializing)/is', $item, $match)) {
                $status = $match[1];
            }

            if (preg_match('/monitoring status\s+(Monitored|Initializing|Not monitored)/is', $item, $match)) {
                $monitoring = $match[1];
            }

            if (preg_match('/pid\s+([0-9]+)/is', $item, $match)) {
                $pid = (int) $match[1];
            }

            $processes[] = new MonitProcess($id, $status, $monitoring, $pid);
        }

        return $processes;
    }

    /**
     * @return bool
     */
    public function isRunning()
    {
        exec('pgrep monit', $pids);
        return !empty($pids);
    }

    /**
     * @return void
     */
    public function stopAll()
    {
        $this->killAllByPidFiles();

        $processes = $this->getRunningProcesses();

        foreach ($processes as $process) {
            $this->monitorProcess($process);
            $this->stopProcess($process);
        }
    }

    /**
     * @return void
     */
    public function startAll()
    {
        if (!$this->isRunning()) {
            exec(MonitProcess::BIN);
            return;
        }

        $processes = $this->getRunningProcesses();

        foreach ($processes as $process) {
            $this->monitorProcess($process);
            $this->restartProcess($process);
        }
    }

    /**
     * @return void
     */
    public function reload()
    {
        if (!$this->isRunning()) {
            return;
        }

        exec(MonitProcess::BIN . ' reload');
    }

    /**
     * @return void
     */
    public function writeConfig()
    {
        $cfg = '';
        $cfg .= 'set daemon  5' . PHP_EOL;
        $cfg .= 'set logfile syslog' . PHP_EOL;
        $cfg .= 'set httpd port 2812 and ' . PHP_EOL .
                    'allow 127.0.0.1' . PHP_EOL .
                    'allow 192.168.1.0/24' . PHP_EOL .
                    'allow admin:monit' . PHP_EOL;

        for ($i = 1; $i <= $this->getInstances(); $i++) {
            $cfg .= 'check process queue' . $i . ' pidfile "' . Yii::getAlias('@runtime/queue' . $i . '.pid') . '"' . PHP_EOL;
            $cfg .= '    start = "/usr/bin/env php ' . Yii::getAlias('@app') . '/yii queue/start queue' . $i . '"' . PHP_EOL;
            $cfg .= '    stop = "/usr/bin/env php ' . Yii::getAlias('@app') . '/yii queue/stop queue' . $i . '"' . PHP_EOL;
            $cfg .= '    if does not exist then start group queue' . PHP_EOL;
        }

        file_put_contents($this->getSysConfigFile(), $cfg);
    }

    /**
     * @param IMonitProcess $process
     */
    public function stopProcess(IMonitProcess $process)
    {
        if ($process->isStopped()) {
            return;
        }

        $process->stop();
    }

    /**
     * @param IMonitProcess $process
     */
    public function startProcess(IMonitProcess $process)
    {
        if ($process->isRunning()) {
            return;
        }

        if ($process->isStarting()) {
            $this->stopProcess($process);
        }

        $process->start();
    }

    /**
     * @param IMonitProcess $process
     */
    public function restartProcess(IMonitProcess $process)
    {
        if ($process->isStarting()) {
            $process->stop();
        }

        $process->restart();
    }

    /**
     * @param IMonitProcess $process
     */
    public function monitorProcess(IMonitProcess $process)
    {
        $process->monitor();
    }

    /**
     * @param IMonitProcess $process
     */
    public function unMonitorProcess(IMonitProcess $process)
    {
        $process->unMonitor();
    }

    /**
     *
     */
    public function killAllByPidFiles()
    {
        $dp = dir(Yii::getAlias('@runtime'));
        while (false !== ($entry = $dp->read())) {
            if (!preg_match('/^queue[0-9]\.pid$/is', $entry)) {
                continue;
            }
            exec('kill -9 ' . file_get_contents(Yii::getAlias('@runtime') . '/' . $entry));
            unlink(Yii::getAlias('@runtime') . '/' . $entry);
        }
        $dp->close();
    }
}
