<?php

namespace semako\monit\components;

use semako\monit\interfaces\IMonitProcess;
use yii;

/**
 * Class MonitProcess
 * @package semako\monit\components
 */
class MonitProcess implements IMonitProcess
{
    const BIN = '/usr/bin/env monit';

    const STATUS_NOT_MONITORED = 1;
    const STATUS_RUNNING = 2;
    const STATUS_INIT_RESTART_PENDING = 3;
    const STATUS_INIT = 4;

    const MONITORING_MONITORED = 1;
    const MONITORING_INIT = 2;
    const MONITORING_NOT_MONITORED = 3;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $status = self::STATUS_NOT_MONITORED;

    /**
     * @var int
     */
    private $monitoring = self::MONITORING_NOT_MONITORED;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int|null
     */
    private $pid;

    /**
     * @var string|null
     */
    private $pidFile;

    /**
     * MonitProcess constructor.
     * @param int $id
     * @param string $status
     * @param string $monitoring
     * @param int|null $pid
     */
    public function __construct($id, $status, $monitoring, $pid = null)
    {
        $this->id   = (int) $id;
        $this->pid  = $pid;
        $this->name = 'queue' . $this->getId();

        $pidFile = Yii::getAlias('@runtime/' . $this->getName() . '.pid');
        if (!is_null($this->getPid()) && file_exists($pidFile) && trim(file_get_contents($pidFile) == $this->getPid())) {
            $this->pidFile = $pidFile;
        }

        switch ($status) {
            case 'Running':
                $this->status = self::STATUS_RUNNING;
                break;

            case 'Initializing - restart pending':
                $this->status = self::STATUS_INIT_RESTART_PENDING;
                break;

            case 'Initializing':
                $this->status = self::STATUS_INIT;
                break;

            case 'Not monitored':
                $this->status = self::STATUS_NOT_MONITORED;
                break;
        }

        switch ($monitoring) {
            case 'Monitored':
                $this->monitoring = self::MONITORING_MONITORED;
                break;

            case 'Initializing':
                $this->monitoring = self::MONITORING_INIT;
                break;

            case 'Not monitored':
                $this->monitoring = self::MONITORING_NOT_MONITORED;
                break;
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getMonitoring()
    {
        return $this->monitoring;
    }

    /**
     * @return int|null
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @return string|null
     */
    public function getPidFile()
    {
        return $this->pidFile;
    }

    /**
     * @return bool
     */
    public function isRunning()
    {
        return $this->getStatus() == self::STATUS_RUNNING;
    }

    /**
     * @return bool
     */
    public function isStarting()
    {
        return $this->getStatus() == self::STATUS_INIT || $this->getStatus() == self::STATUS_INIT_RESTART_PENDING || $this->getMonitoring() == self::MONITORING_INIT;
    }

    /**
     * @return bool
     */
    public function isStopped()
    {
        return $this->getStatus() == self::STATUS_NOT_MONITORED || $this->getMonitoring() == self::MONITORING_NOT_MONITORED;
    }

    /**
     * @return void
     */
    public function stop()
    {
        exec(self::BIN . ' stop ' . $this->getName());
        if ($this->getPidFile()) {
            exec('kill -9 ' . file_get_contents($this->getPidFile()));
            unlink($this->getPidFile());
        }
    }

    /**
     * @return void
     */
    public function start()
    {
        exec(self::BIN . ' start ' . $this->getName());
    }

    /**
     * @return void
     */
    public function restart()
    {
        exec(self::BIN . ' restart ' . $this->getName());
    }

    /**
     * @return void
     */
    public function monitor()
    {
        exec(self::BIN . ' monitor ' . $this->getName());
    }

    /**
     * @return void
     */
    public function unMonitor()
    {
        exec(self::BIN . ' unmonitor ' . $this->getName());
    }
}
